import hashlib
import os
import pickle
import tarfile
import time
from pathlib import Path
from shutil import rmtree
from tkinter import *
from tkinter import filedialog, messagebox, ttk

import fitz
import numpy as np
import pandas as pd
from loguru import logger
from PIL import Image, ImageTk


def _from_rgb(rgb):
    """translates an rgb tuple of int to a tkinter friendly color code"""
    if type(rgb) == str:
        return rgb
    r, g, b = [int(_c * 255) for _c in rgb]
    return f"#{r:02x}{g:02x}{b:02x}"


# from kg
oom_to_prefix = {0: "kg", 3: "t", 6: "kt", 9: "Mt", 12: "Gt"}


def _make_val_readable(value):
    value *= 1e3
    oom = int(round(np.log10(value))) if value != 0 else 1
    prefix_oom = oom - oom % 3
    val = value * 10 ** (-1 * prefix_oom)
    # cut off after 1 digit - f-strings don't support this, they round
    disp_val = int(val * 10) / 10
    return f"{disp_val:.1f} {oom_to_prefix[prefix_oom]}"


# returns the oom of value wrt kg_value
def get_oom(kg_value, value):
    if float(value) == 0:
        return 1
    frac = int(kg_value) / float(value)
    return int(round(np.log10(frac)))


class ReturnPacketException(Exception):
    pass


class SaveException(Exception):
    pass


class PacketDoneException(Exception):
    pass


class DataHandler:
    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise Exception("Weirdly, the work package does not exist")
        self.tempdir = "/tmp/greenspect_trainings_data/"
        Path(self.tempdir).mkdir(parents=True, exist_ok=True)

        self.w_path = self.tempdir + "td_work_package/"
        self.package_path = filename

        # unpack file
        client_tar = tarfile.open(self.package_path)
        client_tar.extractall(self.tempdir)
        client_tar.close()

        # reading work package info
        self.info = pd.read_pickle(self.w_path + "work_info.gz")

        if filename[:-7].endswith("_full"):
            raise ReturnPacketException(
                "Can't work on return packages. Please select another one."
            )
        elif filename[:-7].endswith("_part"):
            self.result = pd.read_pickle(self.w_path + "return_data.gz")
            # get index of first Nan in packet
            self.doc_idx = np.arange(len(self.result))[self.result.oom_value.isna()][0]
            # remove "_part" suffix to save as "_part.tar.gz"
            self.package_path = filename[:-12] + ".tar.gz"
        else:
            self.doc_idx = 0
            self.result = pd.DataFrame(
                index=self.info.index,
                columns=[
                    "company_index",
                    "tabular",
                    "rect_list_value",
                    "oom_value",
                    "rect_list_unit",
                    "rect_list_year",
                    "rect_list_keyword",
                    "rect_list_unit_hint",
                    "fix_in_wr",
                    "manual_data",
                ],
            )
            self.result["company_index"] = self.info["company_index"]
            dtype_dict = {
                "company_index": np.int32,
                "tabular": object,
                "rect_list_value": object,
                "oom_value": np.float64,
                "rect_list_unit": object,
                "rect_list_year": object,
                "rect_list_keyword": object,
                "rect_list_unit_hint": object,
                "fix_in_wr": bool,
                "manual_data": str,
            }
            self.result = self.result.astype(dtype_dict)
            self.result["manual_data"] = ""

        self.load_doc_idx(self.doc_idx)

    def __del__(self):
        # clean up
        rmtree(self.tempdir, ignore_errors=True)

    # load the page of a specified document
    def load_doc_idx(self, _idx):
        self.doc = fitz.Document(self.w_path + self.info.iloc[_idx].ex_file)
        assert self.doc.page_count == 1
        self.page = self.doc.loadPage(0)

        self.r_info = pickle.load(
            open(self.w_path + self.info.iloc[_idx].ex_resultfile, "rb")
        )

    def get_data(self):
        # return info, fitz.Document, rect dict
        return self.info.iloc[self.doc_idx], self.r_info.copy()

    # load the next page
    def load_next_page(self):
        if self.doc_idx < len(self.info) - 1:
            self.doc_idx += 1
            self.load_doc_idx(self.doc_idx)
        else:
            raise PacketDoneException("No more pages in work packet")

    # save result from Application in self.result
    # tabular 0 - invalid, 1 - table, 2 - no table
    @logger.catch
    def save_result(self, locations_found, tabular, fix_in_wr=False):
        _idx = self.info.index[self.doc_idx]
        _save_dict = {
            "value_locations": "rect_list_value",
            "year_locations": "rect_list_year",
            "unit_locations": "rect_list_unit",
            "keyword_locations": "rect_list_keyword",
            "unit_hint_locations": "rect_list_unit_hint",
        }
        if (
            not {"value_locations", "unit_locations"}.issubset(
                set(locations_found.keys())
            )
        ) or fix_in_wr:
            self.result.at[_idx, "rect_list_value"] = [fitz.Rect()]
            self.result.loc[_idx, "oom_value"] = -2.0
            self.result.at[_idx, "rect_list_unit"] = [fitz.Rect()]
            self.result.at[_idx, "rect_list_year"] = [fitz.Rect()]
            self.result.at[_idx, "rect_list_keyword"] = [fitz.Rect()]
            self.result.at[_idx, "rect_list_unit_hint"] = [fitz.Rect()]
            self.result.loc[_idx, "fix_in_wr"] = fix_in_wr
        else:
            manual_res = ""
            for _key in locations_found.keys():
                try:
                    self.result.at[_idx, _save_dict[_key]] = [
                        loc for _value, loc in locations_found[_key]
                    ]
                except KeyError as e:
                    logger.trace(f"Following key not implemented in return packet: {e}")
                if locations_found[_key][0][0] is None:
                    manual_res += _save_dict[_key] + ","

            self.result.loc[_idx, "manual_data"] = manual_res[:-1]

            if "value_locations" in locations_found.keys():
                if locations_found["value_locations"][0][0] is None:
                    self.result.loc[_idx, "oom_value"] = np.nan
                else:
                    self.result.loc[_idx, "oom_value"] = get_oom(
                        self.info.loc[_idx].Value,
                        max(
                            [
                                _value
                                for _value, loc in locations_found["value_locations"]
                            ]
                        ),  # in rare case of non single rect for emission value, take max value
                    )
            self.result.loc[_idx, "tabular"] = (
                (tabular == 1) if tabular != 0 else np.nan
            )
            self.result.loc[_idx, "fix_in_wr"] = False

    def save_and_close(self):
        savepath = os.path.dirname(os.path.abspath(self.package_path))

        if self.doc_idx < len(self.info) - 1:
            suffix = "_part"
            topdir = "td_work_package/"
            staging_path = self.w_path
        else:
            suffix = "_full"
            topdir = "td_return_data/"
            staging_path = self.tempdir + topdir

        Path(staging_path).mkdir(parents=True, exist_ok=True)
        self.result.to_pickle(staging_path + "return_data.gz")
        _hash = hashlib.sha1()
        _hash.update(str(time.time()).encode("utf-8"))
        with open(staging_path + ".hash", "w") as _f:
            _f.write(_hash.hexdigest())

        tar_name = os.path.basename(self.package_path)[:-7] + suffix + ".tar.gz"
        with tarfile.open(tar_name, "w:gz") as tar:
            tar.add(staging_path, arcname=topdir[:-1])


class Application(Frame):
    def __init__(self, master=None, dimensions=(1000, 1000)):
        super().__init__(master, highlightbackground="black", highlightthickness=1)
        self.zoom_int = 0

        self.min_zoom = -10
        self.max_zoom = 30
        self.max_scale = 3.0
        self.min_scale = 0.5
        self._p_x0, self._p_y0 = 0.0, 0.0

        self.dims = dimensions
        self.canvas_share = 0.6
        self.title_height = 60
        self.progress_f_height = 5

        while True:
            try:
                self.dh = DataHandler(self.open_file())
            except ReturnPacketException:
                pass
            else:
                break

        self.optional_info = [
            "year_locations",
            "keyword_locations",
            "unit_hint_locations",
        ]

        self.color_dict = {
            "value_locations": (1, 0.306, 0.365),
            "keyword_locations": (0.243, 0.549, 0.906),
            "year_locations": (0.165, 0.710, 0.353),
            "unit_locations": (1.000, 0.000, 1.000),
            "unit_hint_locations": (0.600, 0.000, 1.000),
        }

        self.root = master
        self.grid(row=0, column=0, sticky=N + S + W + E)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        # use additional frame which can be destroyed and recreated at redraw
        self._f = Frame(self)
        self._f.grid(row=0, column=0, sticky=N + S + W + E)
        self._f.grid_rowconfigure(0, weight=1)
        self._f.grid_columnconfigure(0, weight=1)

        self.reset_data()

        self.init_screen()

        # with Windows OS
        self.root.bind("<MouseWheel>", self._on_mouse_wheel)
        # with Linux OS
        self.root.bind("<Button-4>", self._on_mouse_wheel)
        self.root.bind("<Button-5>", self._on_mouse_wheel)

        self.root.bind("<Button-1>", self._on_click)
        self.root.bind("<ButtonRelease-1>", self._on_click_release)

        # Linux OS and Windows OS
        self.root.bind("<Control_L>", self._on_ctrl_press)
        self.root.bind("<KeyRelease-Control_L>", self._on_ctrl_release)
        self.root.bind("<Control_R>", self._on_ctrl_press)
        self.root.bind("<KeyRelease-Control_R>", self._on_ctrl_release)
        self.root.bind("<Shift_L>", self._on_ctrl_press)
        self.root.bind("<KeyRelease-Shift_L>", self._on_ctrl_release)
        self.root.bind("<Shift_R>", self._on_ctrl_press)
        self.root.bind("<KeyRelease-Shift_R>", self._on_ctrl_release)

        self.root.bind("<B1-Motion>", self._on_left_drag)
        self.bind("<Configure>", self._on_resize)

    # reset all info which is to be determined by the user
    def reset_data(self):
        self.continuous_drag = False
        self.rect_select_type = ""
        self.rect_select = None
        self.manual_locations_found = {}
        self.ctrl_is_pressed = False
        self.locations_found = {}
        # radio button choice, 0 = Invalid, 1 = value in table, 2 = no table
        self.r_choice = IntVar()
        self.r_choice.set(0)
        self.fix_in_wr = False
        self.clip_rect = fitz.Rect(self.dh.page.rect)
        self.info, self.obj_full = self.dh.get_data()
        self.obj_curr = self.obj_full.copy()
        self.to_be_found = []
        for _key in self.obj_full.keys():
            if len(self.obj_full[_key].keys()) > 0:
                self.to_be_found.append(_key)
        self.not_yet_found = self.to_be_found.copy()

    def init_screen(self):
        self._f.destroy()
        self._f = Frame(self)
        self._f.grid(row=0, column=0, sticky=N + S + W + E)
        self._f.grid_rowconfigure(0, weight=1)
        self._f.grid_columnconfigure(0, weight=1)

        self.create_widgets()
        self.progress_bar["value"] = (self.dh.doc_idx + 1) / len(self.dh.info) * 100
        self.tkimg = self._render_scaled_page()
        self.pdf_img = self.canvas.create_image(0, 0, image=self.tkimg, anchor=NW)
        self.redraw_rectangles()

    # GUI is separated into multiple frames, which contain elements.
    # Outermost are "self" and dummy frame "_f" to enable destruction of content.
    # Inside are Title frame in row 0 and Canvas and toolbar frame in row 1.
    # In row 2, the progress indicator (bar and label) are located
    def create_widgets(self):
        # get current dims values
        self.root.update()
        self.dims = self.root.winfo_width(), self.root.winfo_height()

        self.t_frame = Frame(
            self._f,
            height=self.title_height,
            highlightthickness=1,
            highlightbackground="black",
        )
        self.t_frame.grid(row=0, column=0, columnspan=2, sticky=N + S + W + E)
        self.t_frame.columnconfigure(0, weight=1)
        self.t_frame.rowconfigure(0, weight=1)

        _readable_value = _make_val_readable(self.info.Value)
        _readable_metric = ""
        _s1 = "Scope 1"
        _s2 = "Scope 2"
        _s3 = "Scope 3"
        if _s1 in self.info.Metric:
            _readable_metric = _s1
        elif _s2 in self.info.Metric:
            _readable_metric = _s2
        elif _s3 in self.info.Metric:
            _readable_metric = _s3
        else:
            _readable_metric = self.info.Metric

        # fill title frame
        self.title_label = Label(
            self.t_frame,
            padx=10,
            text=(
                f"Please find the {self.info.Year} {_readable_metric} emissions"
                f" ({_readable_value}) for {self.info.Company}"
            ),
            font=("Courier Bold", 15),
        )
        self.title_label.grid(row=0, column=0, sticky=W)
        _c_b_a = {
            "bg": "red",
            "activebackground": "red",
        }
        _c_b_sq = {
            "bg": "orange",
            "activebackground": "orange",
        }
        self.title_button_a = Button(
            self.t_frame,
            text="Abort",
            font="DejaVuSans 9 bold",
            command=self._button_abort,  # , **_c_b_a
        )
        self.title_button_a.grid(row=0, column=1, sticky=N + S + E)
        self.title_button_s_q = Button(
            self.t_frame,
            text="Save & Quit",
            font="DejaVuSans 9 bold",
            command=self._button_save_and_close,
            # **_c_b_sq,
        )
        self.title_button_s_q.grid(row=0, column=2, sticky=N + S + E)
        self.t_frame.rowconfigure(0, weight=1)
        self.t_frame.columnconfigure(0, weight=1)

        self.canvas_frame = Frame(
            self._f, highlightbackground="black", highlightthickness=1
        )
        self.canvas_frame.grid(row=1, column=0, sticky=N + W + S + E)
        self.canvas = Canvas(
            self.canvas_frame,
            width=self.dims[0] * self.canvas_share,
            height=self.dims[1] - self.title_height - self.progress_f_height,
        )
        self.canvas.grid(sticky=N + W + S + E)
        self.canvas_frame.rowconfigure(0, weight=1)
        self.canvas_frame.columnconfigure(0, weight=1)

        self.toolbar_frame = Frame(
            self._f,
            width=self.dims[0] * (1 - self.canvas_share),
            highlightbackground="black",
            highlightthickness=1,
        )
        self.toolbar_frame.grid(row=1, column=1, sticky=N + W + S + E)
        self.toolbar_frame.columnconfigure(0, weight=1)
        self.toolbar_frame.rowconfigure(0, weight=1)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)

        self.legend_frame = LabelFrame(
            self.toolbar_frame, text="Legend", padx=5, pady=2
        )
        self.legend_frame.pack(fill="x", side=TOP, padx=2)

        _c_l_buttons = {
            "bg": "white",
            "activeforeground": "white",
        }
        self.l_em = Button(
            self.legend_frame,
            text="Emissions value",
            font="Helvetica 9 bold",
            command=lambda: self._button_l("value_locations"),
            activebackground=_from_rgb(self.color_dict["value_locations"]),
            fg=_from_rgb(self.color_dict["value_locations"]),
            **_c_l_buttons,
        )
        self.l_em.pack(side=TOP, fill="x")
        self.l_u = Button(
            self.legend_frame,
            text="Emissions unit",
            font="Helvetica 9 bold",
            command=lambda: self._button_l("unit_locations"),
            activebackground=_from_rgb(self.color_dict["unit_locations"]),
            fg=_from_rgb(self.color_dict["unit_locations"]),
            **_c_l_buttons,
        )
        self.l_u.pack(side=TOP, fill="x")
        self.l_yr = Button(
            self.legend_frame,
            text="Year",
            font="Helvetica 9 bold",
            command=lambda: self._button_l("year_locations"),
            activebackground=_from_rgb(self.color_dict["year_locations"]),
            fg=_from_rgb(self.color_dict["year_locations"]),
            **_c_l_buttons,
        )
        self.l_yr.pack(side=TOP, fill="x")
        self.l_kw = Button(
            self.legend_frame,
            text="Keyword (opt)",
            font="Helvetica 9 bold",
            command=lambda: self._button_l("keyword_locations"),
            activebackground=_from_rgb(self.color_dict["keyword_locations"]),
            fg=_from_rgb(self.color_dict["keyword_locations"]),
            **_c_l_buttons,
        )
        self.l_kw.pack(side=TOP, fill="x")
        self.l_uh = Button(
            self.legend_frame,
            text="Unit hint (opt)",
            font="Helvetica 9 bold",
            command=lambda: self._button_l("unit_hint_locations"),
            activebackground=_from_rgb(self.color_dict["unit_hint_locations"]),
            fg=_from_rgb(self.color_dict["unit_hint_locations"]),
            **_c_l_buttons,
        )
        self.l_uh.pack(side=TOP, fill="x")
        self.legend_button_finder = {
            "value_locations": self.l_em,
            "unit_locations": self.l_u,
            "year_locations": self.l_yr,
            "keyword_locations": self.l_kw,
            "unit_hint_locations": self.l_uh,
        }

        self.b_frame = Frame(self.toolbar_frame)
        self.b_frame.pack(side=BOTTOM, fill="x")
        _c_not_found = {
            "bg": "red",
            "activebackground": "red",
            "fg": "white",
            "activeforeground": "white",
        }
        self.b_w_val = Button(
            self.b_frame,
            text="Reported value\nmismatch",
            command=self._button_w_val,
            **_c_not_found,
        )
        self.b_w_val.pack(fill="x")
        self.b_no_val = Button(
            self.b_frame,
            text="Value not found",
            command=self._button_no_val,
            **_c_not_found,
        )
        self.b_no_val.pack(fill="x")
        self.r_label = Label(self.b_frame, text="Value in table?")
        self.r_label.pack(fill="x")
        self.radios = []
        for _text, _var in [("yes", 1), ("no ", 2)]:
            self.radios.append(
                Radiobutton(
                    self.b_frame,
                    text=_text,
                    padx=20,
                    variable=self.r_choice,
                    value=_var,
                ).pack(fill="x")
            )
        _c_b_n = {
            "bg": "green",
            "activebackground": "green",
        }
        self.b_next = Button(
            self.b_frame,
            text="Next",
            state=DISABLED,
            command=self._button_next,
            padx=40,
            pady=40,
            **_c_b_n,
        )
        self.b_next.pack(fill="x")

        self.progress_f = Frame(
            self._f,
            height=self.progress_f_height,
            relief=SUNKEN,
            highlightbackground="black",
            highlightthickness=1,
        )
        self.progress_f.grid(row=2, column=0, columnspan=2, sticky=N + S + W + E)
        self.progress_f.columnconfigure(0, weight=1)
        self.progress_f.rowconfigure(0, weight=1)
        self.progress_lab = Label(
            self.progress_f, text=self._progr_text(), relief=SUNKEN
        )
        self.progress_lab.pack(side="right", fill="y")
        self.progress_lab.update()
        w = self.progress_lab.winfo_width()
        self.progress_bar = ttk.Progressbar(
            self.progress_f, orient=HORIZONTAL, length=self.dims[0] - w
        )
        self.progress_bar.pack(side="left", fill="y")

    def _progr_text(self):
        return f"Page {self.dh.doc_idx+1}/{len(self.dh.info)}"

    def open_file(self, title="Select work package"):
        return filedialog.askopenfilename(
            title=title, filetypes=[("Work packages", "*.tar.gz")]
        )

    def _button_abort(self):
        del self.dh
        self.root.destroy()

    def _button_save_and_close(self):
        self.dh.save_and_close()
        del self.dh
        self.root.destroy()

    def _load_new_page(self):
        # reset origin
        self._p_x0, self._p_y0 = 0.0, 0.0
        # save result
        self.dh.save_result(
            {**self.locations_found, **self.manual_locations_found},
            self.r_choice.get(),
            self.fix_in_wr,
        )

        try:
            self.dh.load_next_page()
            get_new_dh = False
        except PacketDoneException:
            ans = messagebox.askquestion(
                "Success",
                (
                    "This work packet is complete. Thanks for helping Greenspect!\n"
                    "Do you want to continue with a new packet?"
                ),
            )
            # treat "no" as save and close
            if not ans == "yes":
                self._button_save_and_close()
                sys.exit(0)
            self.dh.save_and_close()
            del self.dh
            get_new_dh = True

        # somehow garbage collect only works after except block
        # so we have to do this hack
        if get_new_dh:
            while True:
                try:
                    self.dh = DataHandler(self.open_file("Select next work package"))
                except ReturnPacketException:
                    pass
                else:
                    break
        self.reset_data()
        self.root.update()
        self.dims = self.root.winfo_width(), self.root.winfo_height()
        self.init_screen()
        self.redraw_rectangles()

    # gets called on button click "Next page"
    def _button_next(self):
        self._load_new_page()

    # get called on button click "WikiRate data is bad, we are searching for wrong val"
    def _button_w_val(self):
        self.fix_in_wr = True
        self._load_new_page()

    def _button_l(self, select_type):
        if not self.rect_select_type:
            self.rect_select_type = select_type
            for key in self.legend_button_finder:
                if not key == select_type:
                    self.legend_button_finder[key]["state"] = DISABLED
            self.root.config(cursor="tcross")
        else:
            self.reset_rect_select()

    # gets called on button click "No value found"
    def _button_no_val(self):
        if len(self.locations_found) != 0 or (self.r_choice.get() != 0):
            ans = messagebox.askquestion(
                "Warning",
                "Are you sure you did not find a value? Some information was entered.",
            )
            if not ans == "yes":
                return
        else:
            ans = messagebox.askquestion(
                "Warning",
                "Are you sure you did not find a value?",
            )
            if not ans == "yes":
                return

        self._load_new_page()

    # gets called on mouse wheel action
    def _on_mouse_wheel(self, event):
        self.old_zoom_int = self.zoom_int
        # respond to Linux or Windows or OSX wheel event (osx = +-1)
        if event.num == 5 or event.delta == -120 or event.delta == -1:
            self.zoom_int -= 1
        if event.num == 4 or event.delta == 120 or event.delta == 1:
            self.zoom_int += 1

        # bound zoom by min/max_zoom
        self.zoom_int = max(self.zoom_int, self.min_zoom)
        self.zoom_int = min(self.zoom_int, self.max_zoom)

        self._update_origin(event.x_root, event.y_root)
        self.tkimg = self._render_scaled_page()
        self.canvas.itemconfig(self.pdf_img, image=self.tkimg)

        self.redraw_rectangles()

    # gets called on Window config (e.g. resize)
    def _on_resize(self, event):
        # get current dims values
        self.root.update()
        self.dims = self.root.winfo_width(), self.root.winfo_height()
        # just draw a new screen with new dims
        self.init_screen()
        self.redraw_rectangles()

    # is called on drag with left mouse button
    def _on_left_drag(self, event):
        # only change rendering if drag is on canvas
        if event.widget == self.canvas:
            if not self.continuous_drag:
                # save starting point at beginning of drag
                self.s_d_x, self.s_d_y = event.x, event.y
                self.s_d_p_x0, self.s_d_p_y0 = self._p_x0, self._p_y0
                self.continuous_drag = True
            else:
                # canvas-space point
                _c_p_x = event.x_root - self.canvas.winfo_rootx()
                _c_p_y = event.y_root - self.canvas.winfo_rooty()
                # current effective drag
                _c_e_d_x = _c_p_x - self.s_d_x
                _c_e_d_y = _c_p_y - self.s_d_y
                scale = self._zoom_int_to_scale()
                if self.rect_select_type:
                    self._r_s_x1 = max(_c_p_x, 0.0)
                    self._r_s_y1 = max(_c_p_y, 0.0)
                    x_offset = max(self.clip_rect.x0, 0.0)
                    y_offset = max(self.clip_rect.y0, 0.0)
                    self.rect_select = fitz.Rect(
                        self.s_d_x / scale + x_offset,
                        self.s_d_y / scale + y_offset,
                        self._r_s_x1 / scale + x_offset,
                        self._r_s_y1 / scale + y_offset,
                    )
                    self.rect_select *= self.dh.page.derotation_matrix
                else:
                    self._p_x0 = max(self.s_d_p_x0 - _c_e_d_x / scale, 0.0)
                    self._p_y0 = max(self.s_d_p_y0 - _c_e_d_y / scale, 0.0)

            if not self.rect_select_type:
                self.tkimg = self._render_scaled_page()
                self.canvas.itemconfig(self.pdf_img, image=self.tkimg)
            self.redraw_rectangles()

    def reset_rect_select(self):
        self.root.config(cursor="arrow")
        for legend_button in self.legend_button_finder.values():
            legend_button["state"] = NORMAL
        if self.rect_select:
            if self.rect_select_type in self.manual_locations_found:
                self.manual_locations_found[self.rect_select_type].append(
                    (None, self.rect_select)
                )
            else:
                self.manual_locations_found[self.rect_select_type] = [
                    (None, self.rect_select)
                ]
            if self.rect_select_type in self.not_yet_found:
                self.not_yet_found.remove(self.rect_select_type)
            else:
                self.locations_found.pop(self.rect_select_type, None)
        self.rect_select_type = ""
        self.rect_select = None

    def _on_click_release(self, event):
        self.continuous_drag = False
        if self.rect_select_type and self.rect_select is not None:
            self.reset_rect_select()
        self.redraw_rectangles()
        self.update_button()

    # is called on click and drag
    def _on_click(self, event):
        x_offset = max(self.clip_rect.x0, 0.0)
        y_offset = max(self.clip_rect.y0, 0.0)
        scale = self._zoom_int_to_scale()

        # search all not yet selected rectangles for match
        found_new_value = False
        if self.ctrl_is_pressed:
            selectable = self.to_be_found
        else:
            selectable = self.not_yet_found

        for _type in selectable:
            for _value in self.obj_curr[_type]:
                for idx, loc in enumerate(self.obj_curr[_type][_value]):
                    _loc = loc * self.dh.page.rotation_matrix
                    scaled_loc = fitz.Rect(
                        scale * (_loc.x0 - x_offset),
                        scale * (_loc.y0 - y_offset),
                        scale * (_loc.x1 - x_offset),
                        scale * (_loc.y1 - y_offset),
                    )
                    if scaled_loc.contains((event.x, event.y)):
                        try:
                            if _type in self.not_yet_found:
                                self.not_yet_found.remove(_type)
                            if not _type in self.locations_found:
                                self.locations_found[_type] = [(_value, loc)]
                                found_new_value = True
                            else:
                                if not (_value, loc) in self.locations_found[_type]:
                                    self.locations_found[_type].append((_value, loc))
                                    found_new_value = True
                        except ValueError as e:  # click overlapping rects -> double remove
                            logger.exception(e)

            if found_new_value and not self.ctrl_is_pressed:
                self.manual_locations_found.pop(_type, None)

        # check if this is deselection rather than selection of rect
        if not found_new_value:
            to_be_deleted = ""
            for _type, loc_list in self.locations_found.items():
                for (_value, loc) in reversed(loc_list):
                    _loc = loc * self.dh.page.rotation_matrix
                    scaled_loc = fitz.Rect(
                        scale * (_loc.x0 - x_offset),
                        scale * (_loc.y0 - y_offset),
                        scale * (_loc.x1 - x_offset),
                        scale * (_loc.y1 - y_offset),
                    )
                    if scaled_loc.contains((event.x, event.y)):
                        if self.ctrl_is_pressed:
                            try:
                                self.locations_found[_type].remove((_value, loc))
                            except:
                                logger.exception("Unexpected behavior")
                            # check if list is empty now:
                            if not self.locations_found[_type]:
                                self.not_yet_found.append(_type)
                                to_be_deleted = _type
                        else:
                            self.not_yet_found.append(_type)
                            to_be_deleted = _type

            _break = False
            for _type, loc_list in self.manual_locations_found.items():
                # delete youngest first
                for (_value, loc) in reversed(loc_list):
                    _loc = loc * self.dh.page.rotation_matrix
                    scaled_loc = fitz.Rect(
                        scale * (_loc.x0 - x_offset),
                        scale * (_loc.y0 - y_offset),
                        scale * (_loc.x1 - x_offset),
                        scale * (_loc.y1 - y_offset),
                    )
                    if scaled_loc.contains((event.x, event.y)):
                        self.manual_locations_found[_type].remove((_value, loc))
                        if len(self.manual_locations_found[_type]) == 0:
                            self.not_yet_found.append(_type)
                            to_be_deleted = _type
                        _break = True
                    if _break:
                        break
                if _break:
                    break
            if to_be_deleted:
                self.locations_found.pop(to_be_deleted, None)
                self.manual_locations_found.pop(to_be_deleted, None)

        self.redraw_rectangles()
        self.update_button()
        # reset found_new_value
        found_new_value = False

    def _on_ctrl_press(self, event):
        self.ctrl_is_pressed = True
        self.redraw_rectangles()

    def _on_ctrl_release(self, event):
        self.ctrl_is_pressed = False
        self.redraw_rectangles()

    # update "Next" button depending on progress of tagging
    def update_button(self):
        if self.r_choice.get() != 0:
            if len(self.not_yet_found) == 0:
                self.b_next["state"] = NORMAL
            elif len(self.not_yet_found) <= 2 and set(self.not_yet_found).issubset(
                self.optional_info
            ):
                self.b_next["state"] = NORMAL
            else:
                self.b_next["state"] = DISABLED
        else:
            self.b_next["state"] = DISABLED

    def redraw_rectangles(self):
        # delete all rectangles (rect and images tagged "rectangle")
        self.canvas.delete("rectangle")
        self.canvas.delete("rect_select")
        # draw rectangles
        self.add_rectangles()

    def _update_origin(self, x_canvas_r, y_canvas_r):
        scale = self._zoom_int_to_scale()
        scale_old = self._zoom_int_to_scale(self.old_zoom_int)

        # update to get current w, h values
        self.canvas.update()
        w, h = (self.canvas.winfo_width(), self.canvas.winfo_height())

        self.canvas.update()
        # canvas-space x/y coords of pointer
        _c_p_x = x_canvas_r - self.canvas.winfo_rootx()
        _c_p_y = y_canvas_r - self.canvas.winfo_rooty()
        assert _c_p_x > 0
        assert _c_p_y > 0

        # old page-space x/y coords of pointer
        _p_p_x = self._p_x0 + _c_p_x / scale_old
        _p_p_y = self._p_y0 + _c_p_y / scale_old
        # new page-space x0/y0 coords (minimum is 0.,0. due to pixmap generation)
        self._p_x0 = max(_p_p_x - _c_p_x / scale, 0.0)
        self._p_y0 = max(_p_p_y - _c_p_y / scale, 0.0)

    # render page depending on _p_x0, _p_y0 and zoom level
    def _render_scaled_page(self, coords=None):
        scale = self._zoom_int_to_scale()
        _matrix = fitz.Matrix(1, 1).preScale(scale, scale)

        # update to get current w, h values
        self.canvas.update()
        w, h = (self.canvas.winfo_width(), self.canvas.winfo_height())

        # get page-space clip rectangle
        self.clip_rect = fitz.Rect(
            self._p_x0, self._p_y0, self._p_x0 + w / scale, self._p_y0 + h / scale
        )
        pix = self.dh.page.getPixmap(matrix=_matrix, clip=self.clip_rect)

        mode = "RGBA" if pix.alpha else "RGB"
        return ImageTk.PhotoImage(
            Image.frombytes(mode, [pix.width, pix.height], pix.samples)
        )

    # create rectangle or image-rect if alpha is given as canvas can't do transparency
    def create_rectangle(self, x1, y1, x2, y2, **kwargs):
        if "outline" in kwargs:
            kwargs["outline"] = _from_rgb(kwargs["outline"])
        if "alpha" in kwargs:
            if "outline" in kwargs:
                kwargs.pop("outline")
            alpha = int(kwargs.pop("alpha") * 255)
            fill = kwargs.pop("fill")
            fill = (int(fill[0] * 255), int(fill[1] * 255), int(fill[2] * 255), alpha)
            _x1, _y1 = x1, y1
            x1, y1 = min(x1, x2), min(y1, y2)
            x2, y2 = max(_x1, x2), max(_y1, y2)
            img = Image.new("RGBA", (int(x2 - x1) + 1, int(y2 - y1) + 1), color=fill)
            self.images.append(ImageTk.PhotoImage(img))
            self.canvas.create_image(
                x1, y1, image=self.images[-1], anchor="nw", **kwargs
            )
        self.canvas.create_rectangle(x1, y1, x2, y2, **kwargs)

    # add rectangles from categories still to be found and found rectangles separately
    def add_rectangles(self):
        x_offset = max(self.clip_rect.x0, 0.0)
        y_offset = max(self.clip_rect.y0, 0.0)
        scale = self._zoom_int_to_scale()

        if self.ctrl_is_pressed:
            rects_to_show = self.to_be_found
        else:
            rects_to_show = self.not_yet_found

        for _type in rects_to_show:
            color = self.color_dict[_type]
            for value in self.obj_curr[_type]:
                for loc in self.obj_curr[_type][value]:
                    _loc = loc * self.dh.page.rotation_matrix
                    self.create_rectangle(
                        scale * (_loc.x0 - x_offset),
                        scale * (_loc.y0 - y_offset),
                        scale * (_loc.x1 - x_offset),
                        scale * (_loc.y1 - y_offset),
                        tag="rectangle",
                        outline=color,
                        width=1.9 * scale,
                    )

        if self.rect_select is not None:
            _rect_select = self.rect_select * self.dh.page.rotation_matrix
            self.create_rectangle(
                scale * (_rect_select.x0 - x_offset),
                scale * (_rect_select.y0 - y_offset),
                scale * (_rect_select.x1 - x_offset),
                scale * (_rect_select.y1 - y_offset),
                tag="rect_select",
                outline=_from_rgb(self.color_dict[self.rect_select_type]),
                width=1.9,
            )

        self.images = []
        for _type, loc_list in self.manual_locations_found.items():
            color = self.color_dict[_type]
            for (_value, loc) in loc_list:
                _loc = loc * self.dh.page.rotation_matrix
                self.create_rectangle(
                    scale * (_loc.x0 - x_offset),
                    scale * (_loc.y0 - y_offset),
                    scale * (_loc.x1 - x_offset),
                    scale * (_loc.y1 - y_offset),
                    tag="rectangle",
                    fill=color,
                    alpha=0.5,
                )

        for _type, loc_list in self.locations_found.items():
            color = self.color_dict[_type]
            for (_value, loc) in loc_list:
                _loc = loc * self.dh.page.rotation_matrix
                self.create_rectangle(
                    scale * (_loc.x0 - x_offset),
                    scale * (_loc.y0 - y_offset),
                    scale * (_loc.x1 - x_offset),
                    scale * (_loc.y1 - y_offset),
                    tag="rectangle",
                    fill=color,
                    alpha=0.5,
                )

    # bilinear interpol. to max_scale@max_zoom and min_scale@min_zoom with scale(0)=1
    def _zoom_int_to_scale(self, zoom_int=None):
        if zoom_int is None:
            zoom_int = self.zoom_int
        if zoom_int > 0:
            return (self.max_scale - 1) / self.max_zoom * zoom_int + 1
        elif zoom_int == 0:
            return 1
        else:
            return (self.min_scale - 1) / self.min_zoom * zoom_int + 1


@logger.catch
def main():
    init_w, init_h = 1000, 700
    root = Tk()
    root.title("Greenspect - Trainings Data Generator")
    root.geometry(f"{init_w}x{init_h}")
    # set icon
    img = PhotoImage(file="greenspect.png")
    root.tk.call("wm", "iconphoto", root._w, img)

    app = Application(master=root, dimensions=(init_w, init_h))
    app.grid(sticky=N + S + W + E)
    root.rowconfigure(0, weight=1)
    root.columnconfigure(0, weight=1)

    app.mainloop()


if __name__ == "__main__":
    main()
